import path from "path";
export default async ({ app, confs }) => {
  console.log(
    ">>>>> server runtime function <<<<<\n",
    path.join(path.resolve("./"), "RunTime/server/index.js")
  );
};
