import Header from "./components/Header.jsx";
import Body from "./components/Body.jsx";
import SideBar from "./components/SideBar.jsx";

export default ({ children, data, match, ...props }) => {
  const store = Store.useSelector((x) => x);

  const HeaderHeight = 50;
  const [sideBarWidth, setSideBarWidth] = useState(280);
  const [isOpenSildebar, setIsOpenSildebar] = useState(false);

  const [sLang, setSlang] = useState("en");

  return (
    <div style={{ width: "100vw", height: "100vh", overflow: "hidden" }}>
      <Header
        pageTitle="salam page title"
        HeaderHeight={HeaderHeight}
        sideBarWidth={sideBarWidth}
        setIsOpenSildebar={setIsOpenSildebar}
        isOpenSildebar={isOpenSildebar}
        sLang={sLang}
      />
      <div className="d-flex">
        <SideBar
          data={data}
          match={match}
          {...props}
          HeaderHeight={HeaderHeight}
          sideBarWidth={sideBarWidth}
          setIsOpenSildebar={setIsOpenSildebar}
          isOpenSildebar={isOpenSildebar}
        />
        <Body HeaderHeight={HeaderHeight} sideBarWidth={sideBarWidth}>
          {children}
        </Body>
      </div>
    </div>
  );
};
