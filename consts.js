export default {
  octaEndPoint: "http://localhost:4000",

  type: "user", // user | octa و در حالت اوکتا اپلیکیشنها در یک نرم افزار مستقل اجرا خواهند شد که مدیریت ان با اوکتا میباشد و اکانتها به هیچ عنوان قابلیت مشاهده ندارند حالت کاربر یعنی قابلیت مدیریت اپلیکیشنها را دارد
  //   dbUrl: "mongodb://localhost:27017/OctaTest",

  isOctaVersion: false, // درصورتی که اپلیکیشن مادر باشد و روی سرور اکتانیوم ران شود
  // temp: "octaErp___",
  cookieName: "OCTA_ERP",
};
