import fs from "fs";
import path from "path";

export default (req, res) => {
  try {
    const root = path.resolve("./Langs");

    const langs = req.body;

    for (const key in langs) {
      const thisFile = path.join(root, `${key}.json`);
      if (!fs.existsSync(thisFile)) {
        fs.writeFileSync(thisFile, JSON.stringify(langs[key], null, 2));
      } else {
        const oldDate = JSON.parse(fs.readFileSync(thisFile));

        const thisLang = langs[key];

        for (const langRootKey in thisLang) {
          if (langRootKey === "items") continue;
          oldDate[langRootKey] = thisLang[langRootKey];
        }

        if (!oldDate.items) oldDate.items = {};

        for (const itemKey in thisLang.items)
          oldDate.items[itemKey] = thisLang.items[itemKey];

        fs.writeFileSync(thisFile, JSON.stringify(oldDate, null, 2));
      }
    }

    res.send(true);
  } catch (error) {
    console.log(error);
    res.send(false);
  }
};
