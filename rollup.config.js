import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";
import multi from "@rollup/plugin-multi-entry";

//@rollup/plugin-dynamic-import-vars //TODO

export default {
  input: ["a.js", "b.js"],
  output: {
    dir: "build",
    format: "cjs",
  },
  plugins: [nodeResolve(), commonjs(), json(), multi()],
};
